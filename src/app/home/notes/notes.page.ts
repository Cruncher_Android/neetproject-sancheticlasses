import { Component, OnInit } from "@angular/core";
import { SubjectsService } from "src/app/services/subjects.service";

@Component({
  selector: "app-notes",
  templateUrl: "./notes.page.html",
  styleUrls: ["./notes.page.scss"],
})
export class NotesPage implements OnInit {
  subjects: { subjectId: null; subjectName: ""; imagePath: "" }[] = [];


  constructor(
    private subjectsService: SubjectsService
  ) {}

  ngOnInit() {
    this.subjectsService.loadSubjects().then((subjects) => {
      this.subjects = subjects;
    });
  }

}
