import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookmarkChaptersPageRoutingModule } from './bookmark-chapters-routing.module';

import { BookmarkChaptersPage } from './bookmark-chapters.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookmarkChaptersPageRoutingModule
  ],
  declarations: [BookmarkChaptersPage]
})
export class BookmarkChaptersPageModule {}
