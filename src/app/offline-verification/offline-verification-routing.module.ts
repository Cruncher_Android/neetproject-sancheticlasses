import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OfflineVerificationPage } from './offline-verification.page';

const routes: Routes = [
  {
    path: '',
    component: OfflineVerificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfflineVerificationPageRoutingModule {}
