import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class TestSeriesPcmMarksService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS testSeries_pcm_marks
        (test_id integer primary key,p_mark1 integer,c_mark1 integer ,m_mark1 integer,p_mark2 integer,c_mark2 integer ,
          m_mark2 integer,p_mark3 integer,c_mark3 integer ,m_mark3 integer,p_mark4 integer,c_mark4 integer ,m_mark4 integer,
            p_mark5 integer,c_mark5 integer,m_mark5 integer`, []).then( _ => {
              this.databaseServiceService.getDataBase().executeSql(`INSERT INTO testSeries_pcm_marks
              (test_id,p_mark1,c_mark1,m_mark1,p_mark2,c_mark2,m_mark2,p_mark3,c_mark3 ,m_mark3,p_mark4,c_mark4,m_mark4,p_mark5,c_mark5,m_mark5)
              values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
          });
       }
    });
  }
}

