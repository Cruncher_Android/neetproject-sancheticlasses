import { DatabaseServiceService } from './database-service.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  loadSubjects() {
    return this.databaseServiceService.getDataBase().executeSql('SELECT * FROM subjects', []).then( result => {
      const subjects = [];
      if (result.rows.length > 0) {
        for (let i = 0; i < result.rows.length; i++) {
           subjects.push({
            subjectId: result.rows.item(i).subjectId,
            subjectName: result.rows.item(i).subjectName,
            imagePath: result.rows.item(i).imagePath,
           });
        }
      }
      return subjects;
    });
  }
}
